package info

// Project, Version, Build and Timestamp are set from environment variables via load flags at build time:
//    GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD) && \
//    GIT_COMMIT=$(git rev-list -1 HEAD) && \
//    GIT_PROJECT=$(git config --local remote.origin.url|sed -n 's#.*/\([^.]*\)\.git#\1#p') && \
//    TIMESTAMP=$(date -u +"%Y-%m-%d %H:%M:%S")
//    go build -ldflags "-X info.Version=$GIT_BRANCH -X info.Build=$GIT_COMMIT -X info.Project=$GIT_PROJECT -X info.Timestamp=$TIMESTAMP" ...
var (
	Project   string
	Version   string
	Build     string
	Timestamp string
)

// String returns the info string
func String() (s string) {
	if Project != "" {
		s = Project + " "
	}

	var openBracket bool
	if Version != "" {
		if s == "" {
			s = "version " + Version
		} else {
			s += "(version " + Version
			openBracket = true
		}
	}

	if Build != "" {
		if s == "" {
			s = "build " + Build
		} else if Version != "" {
			s += ", build " + Build
		} else {
			s += "(build " + Build
			openBracket = true
		}
	}

	if Timestamp != "" {
		if s == "" {
			s = "(build at " + Timestamp
			openBracket = true
		} else {
			s += " at " + Timestamp
		}
	}

	if openBracket {
		s += ")"
	}

	return s
}

// Name returns Project
func Name() (s string) {
	if Project != "" {
		s = Project
	}
	return s
}
